import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class TestLancerDes {


    String s;
    InputParser inputParser;
    InputParser inputParserFalse;


    @Before
    public void init(){
        s = "2d5";
        inputParser = new InputParser(s);
    }

    /**
     * Test si la valeur est numérique
     */
    @Test
    public void TestValeurNumerique(){

        assertTrue("La valeur doit être numerique",inputParser.checkNumeric());
    }

    @Test
    public void TestValeurFaceLimitValid(){
        assertTrue("La valeur des faces doit être entre 2 et 100",inputParser.checkFace());
    }

    @Test
    public void TestValeurDesLimitValid(){
        assertTrue("La valeur des dès doit être possitif",inputParser.checkDes());

    }

    @Test
    public void TestValeurDesLimitInvalid(){
        inputParserFalse = new InputParser("2222222222222222d33333333");
        assertFalse("Doit retourner false",inputParser.checkDes());
    }

    @Test
    public void TestValeurFaceInvalid(){
        inputParserFalse = new InputParser("222222223262622d33333333");
        assertFalse("Doit retourner false",inputParser.checkFace());
    }
}
