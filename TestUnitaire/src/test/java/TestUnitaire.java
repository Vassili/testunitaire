
import org.apache.commons.lang3.StringUtils;
import org.junit.*;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

import java.lang.reflect.Executable;
import java.util.Random;
import java.util.Scanner;

/**
 * Classe de test
 *
 * Executer dans l'ordre alphabetique
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestUnitaire {

    String s1,s2,s3,defaultS;
    int nbFace,nbDes,modificateur;

    @Before
    public void init(){
        s1 = "2d3";
        s2 ="2d3 4d5";
        s3 = "5d9-4";
        defaultS = "";

        nbDes = 2;
        nbFace = 5;

        modificateur = -4;
    }

    /**
     * Tester la synthaxe si c'est numérique
     */
    @Test
    public void A_synthaxeLectureNumerique() throws Exception {
        String[] str = s1.split("-|d");
        boolean numeric = false;


        if(StringUtils.isNumeric(str[0]) && StringUtils.isNumeric(str[1])){

            numeric = true;
        }else{
            throw new Exception("Mauvaise synthaxe");
        }

        assertTrue("Mauvaise synthaxe",StringUtils.isNumeric(str[0])); //Digit
        assertTrue("Mauvaise synthaxe",StringUtils.isNumeric(str[1])); //Digit
    }

    /**
     * Verifie que la valeur des des est entre 0 et 100
     */
    @Test
    public void B_synthaxelectureValeurDes() throws Exception {

        String[] str = s1.split("-|d");

        nbDes = Integer.parseInt(str[0]);
        nbFace = Integer.parseInt(str[1]);
        if( ( nbDes >= 0 && nbDes <= 100) && (nbFace >= 2 && nbFace <= 100)){
            assertTrue("Le nombre de face doit être entre 2 et 100",nbFace >= 2 && nbFace <= 100);
            assertTrue("Le nombre de face doit être entre 0 et 100",nbDes >0 && nbDes <= 100);
        }else{
            throw new Exception("Valeur mauvaise");
        }
    }

    /**
     * Test les lancer de dés avec une limite de 100
     */
    @Test
    public void C_lancerDes(){

        if(nbDes <= 100){
            for(int i=1;i<=nbDes;i++){

                Random r = new Random();
                int nbAlea = r.nextInt((nbFace - 2) + 1) + 2;

                assertTrue("Limite de 100 passé", i < 100);
                assertTrue("Le nombre aléatoire est mauvais",nbAlea > 0 && nbAlea <= 100);
            }
        }
    }

    /**
     * Verifie si la deuxième serie à la bonne synthaxe
     */
    @Test
    public void DA_plusieursSerieSynthaxe(){
        String[] serie = s2.split(" ");
        Boolean numeric = false;

        for (String st : serie){

            String[] str = s1.split("-|d");

            if(StringUtils.isNumeric(str[0]) && StringUtils.isNumeric(str[1])){
                numeric = true;
            }
        }

        assertTrue("Synthaxe mauvais pour la deuxième synthaxe",numeric);

    }

    /**
     * Lancer plusierus serie de test
     */
    @Test
    public void DB_lancerPlusieursSerie(){

        String[] serie = s2.split(" ");
        int[][] resultSerie = new int[serie.length][100];

        int indice1 = 0;

        for (String st : serie) {

            if(StringUtils.isNotEmpty(st)) {
                String[] c = s1.split("-|d");

                int nbDes = Integer.parseInt(c[0]);
                int nbFace = Integer.parseInt(c[1]);


                if(nbDes <= 100){
                    for(int i=0;i<nbDes;i++){

                        Random r = new Random();
                        int nbAlea = r.nextInt((nbFace - 2) + 1) + 2;

                        resultSerie[indice1][i] = nbAlea;

                        assertTrue("Limite de 100 passé", (i < 100));
                        assertTrue("Le nombre aléatoire est mauvais",nbAlea > 0 && nbAlea <= 100);
                    }
                }
            }
            indice1++;
        }

        assertTrue("Le tableau doit contenir des valeurs", resultSerie[0].length > 0);
        assertTrue("Le tableau doit contenir des valeurs", resultSerie[1].length > 0);
    }

    /**
     * Récupére les modificateurs
     */
    @Test
    public void E_recupererModificateur(){
        int modificateur = 0;

        String[] str = s3.split("-|d");

        if(StringUtils.isNotEmpty(str[2])){
            modificateur = Integer.parseInt(str[2]);
        }

        assertTrue("Le modificateur doit être récuperer",modificateur != 0);
    }

    /**
     * Test le modificateur compris entre -100 et 100
     */
    @Test
    public void F_modificateurValeurComprise(){

        int modificateur = 0;

        String[] str = s3.split("-|d");

        modificateur = Integer.parseInt(str[2]);

        if(-100 <= modificateur  && 100 >= modificateur){
            modificateur = Integer.parseInt(str[2]);
        }

        assertTrue("Le modificateur doit être compris entre -100 et 100",-100 <= modificateur && 100>= modificateur);
    }

    /**
     * Test l'assignement d'une valeur par defaut
     */
    @Test
    public void G_valeurParDefaut(){

        if(StringUtils.isEmpty(defaultS)){
            defaultS = "1d6";
        }

        assertEquals("La valeur par defaut est 1d6",defaultS,"1d6");
    }

    /**
     * Le test verifie les erreurs de synthaxe
     */
    @Test
    public void H_erreurSynthaxe(){
        Exception exception = null;
        s1 = "2dx";

        try{
            A_synthaxeLectureNumerique();
            B_synthaxelectureValeurDes();
        }catch (Exception e){
            exception = e;
        }

        assertNotNull("Doit renvoyer une exception",exception);
    }
}
