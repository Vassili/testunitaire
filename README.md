# TestUnitaire

Pour les tests unitaires:
- sudo apt-get install junit

-------- COMPLILE -------------
- java -cp .:/usr/share/java/junit4-4.10.jar CLASSEDETEST 

-----------------RUN ---------
- java -cp /usr/share/java/junit4.jar:. org.junit.runner.JUnitCore CLASSEDETEST 

Pour le projet :
- demande Gradle : https://gradle.org/install/
- JUnit utilisé
- Java 8

Java 8 : apt install openjdk-8-jdk

Gradle: 
    wget https://services.gradle.org/distributions/gradle-5.0-bin.zip -P /tmp
    
    sudo unzip -d /opt/gradle /tmp/gradle-*.zip
    
    sudo nano /etc/profile.d/gradle.sh


Auteur : Vassili BERNARD